import React, {Component, Fragment} from 'react';
import axios from 'axios';
import Button from "../../../Components/UI/Button/Button";
import Spinner from "../../../Components/UI/Spinner/Spinner";
import './EditQuote.css';

class EditQuote extends Component {

  state = {
    author: '',
    category: '',
    text: '',
    loading: false,
  };

  changeAuthorHandler = event => {
    this.setState({author: event.target.value});
  };

  changeTextHandler = event => {
    this.setState({text: event.target.value});
  };

  changeCategoryHandler = event => {
    this.setState({category: event.target.value});
  };



  updateQuote = () => {
    this.setState(() => {
      axios.patch(`/quotes/${this.props.match.params.id}.json`, {
        author: this.state.author,
        category: this.state.category,
        text: this.state.text,
      }).then(() => {
        this.props.history.push('/');
      })
    })
  };

  componentDidMount() {
    this.setState({loading: true}, () => {
      axios.get(`/quotes/${this.props.match.params.id}.json`)
        .then(response => {
          this.setState({
            author: response.data.author,
            category: response.data.category,
            text: response.data.text,
            loading: false,
          })
        })
    });
  };

  render() {
    let editQuote = (
      <Fragment>
        <p className='Title'>Edit a quote</p>
        <div>
          <p style={{color: '#9d9d9d'}}>Category</p>
          <select value={this.state.category}
                  onChange={this.changeCategoryHandler}>
            <option value="Star Wars">Star Wars</option>
            <option value="Famous people">Famous people</option>
            <option value="Saying">Saying</option>
            <option value="Humour">Humour</option>
            <option value="Motivational">Motivational</option>
          </select>
        </div>
        <div>
          <p style={{color: '#9d9d9d'}}>Author</p>
          <input type="text"
                 onChange={this.changeAuthorHandler}
                 value={this.state.author}/>
        </div>
        <div>
          <p style={{color: '#9d9d9d'}}>Quote text</p>
          <textarea value={this.state.text}
                    onChange={this.changeTextHandler} cols="30" rows="10"/>
        </div>
        <Button clicked={() => this.updateQuote()}>Save</Button>
      </Fragment>
    );

    if (this.state.loading) {
      editQuote = <Spinner/>
    }

    return(
      <div className='EditQuote'>
        {editQuote}
      </div>
    );
  }
}

export default EditQuote;