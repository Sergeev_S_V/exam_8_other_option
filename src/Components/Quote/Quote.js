import React from 'react';
import Button from "../UI/Button/Button";
import './Quote.css';

const Quote = props => {
  return(
    <div className='Quote'>
      <div className='ButtonsForm'>
        <Button clicked={props.edit}>Edit</Button>
        <Button clicked={props.delete}>Delete</Button>
      </div>
      <p>Author: {props.author}</p>
      <p>Says: {props.text}</p>
    </div>
  );
};

export default Quote;
