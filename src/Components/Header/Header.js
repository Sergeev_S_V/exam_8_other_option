import React, {Component} from 'react';
import Button from "../UI/Button/Button";
import './Header.css';

class Header extends Component {

  goGeneralPage = () => {
    this.props.history.push('/');
  };

  wantAddNewQuote = () => {
    this.props.history.push('/add-quote');
  };

  render() {
    return (
      <header>
        <p className='Title'>Collection of quotes app</p>
        <div className='Buttons'>
          <Button clicked={() => this.goGeneralPage()}>Quotes</Button>
          <Button clicked={() => this.wantAddNewQuote()}>Submit new quote</Button>
        </div>
      </header>
    );
  }
}

export default Header;